﻿using System.Windows.Forms;
using System.Drawing;
using System;
using System.Windows;

namespace MapVariableTool
{
    class MassBox
    {
        public static readonly Color DEFAULT_COLOR = Color.LightGray;
        public static readonly Color INPUTED_COLOR = Color.GreenYellow;

        public static readonly Size MASS_SIZE = new Size(40, 40);
        public static readonly Point BASE_SPACE = new Point(25, 25);

        private readonly Size TEXT_BOX_SIZE = new Size(20, 20);

        private readonly Size[] WALL_SIZES = { new Size(33, 3), new Size(3, 33) };
        private readonly Size[] DOOR_SIZES = { new Size(27, 6), new Size(6, 27) };
        private readonly Point TEXT_BOX_POS = new Point(10, 10);
        private readonly Point[] WALL_POSES = { new Point(3, 0), new Point(37, 3), new Point(3, 37), new Point(0, 3) };
        private readonly Point[] DOOR_POSES = { new Point(6, 0), new Point(34, 6), new Point(6, 34), new Point(0, 6) };

        private const int LABEL_NUM = 4;

        public PictureBox PictureBox;
        public Label[] Walls;
        public Label[] Doors;
        public TextBox Text;
        private Timer timer;

        private Action callback;

        private static bool isDragAllow;
        private static string currentInput;

        /// <summary>
        /// マスを生成
        /// </summary>
        /// <param name="control"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="massImage"></param>
        /// <param name="save"></param>
        public MassBox(Control control, int x, int y, Image massImage, Action save)
        {
            PictureBox = new PictureBox();
            PictureBox.Size = MASS_SIZE;
            PictureBox.Image = massImage;
            PictureBox.Parent = control;
            PictureBox.Location = new Point(x * MASS_SIZE.Width + BASE_SPACE.X, y * MASS_SIZE.Height + BASE_SPACE.Y);

            timer = new Timer();
            timer.Tick += ClickTimer;
            timer.Interval = 8;
            timer.Start();

            Text = new TextBox();
            Text.Parent = PictureBox;
            Text.Size = TEXT_BOX_SIZE;
            Text.Location = TEXT_BOX_POS;
            Text.Text = "00";
            Text.KeyDown += Text_KeyDown;
            Text.KeyPress += Text_KeyPress;
            Text.Leave += Text_Leave;
            Text.BackColor = DEFAULT_COLOR;
            callback = save;

            Walls = new Label[LABEL_NUM];
            Doors = new Label[LABEL_NUM];
            for (int i = 0; i < Walls.Length; i++)
            {
                Walls[i] = new Label();
                Walls[i].Parent = PictureBox;
                Walls[i].Size = WALL_SIZES[i % WALL_SIZES.Length];
                Walls[i].Location = WALL_POSES[i % WALL_POSES.Length];
                Walls[i].BackColor = DEFAULT_COLOR;

                Doors[i] = new Label();
                Doors[i].Parent = PictureBox;
                Doors[i].Size = DOOR_SIZES[i % DOOR_SIZES.Length];
                Doors[i].Location = DOOR_POSES[i % DOOR_POSES.Length];
                Doors[i].BackColor = DEFAULT_COLOR;
            }
        }

        /// <summary>
        /// エンターキーが押された時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
                Normalize(true);
        }

        /// <summary>
        /// 数値表記をまとめる
        /// </summary>
        /// <param name="isInput"></param>
        public void Normalize(bool isInput = false)
        {
            if (string.IsNullOrEmpty(Text.Text))
            {
                Text.Text = "00";
            }
            int num = Convert.ToInt32(Text.Text, 16) % 256;
            if (num > 0)
                Text.BackColor = INPUTED_COLOR;
            else
                Text.BackColor = DEFAULT_COLOR;
            Text.Text = string.Format("{0:X2}",num);
            if (isInput)
                currentInput = Text.Text;
            callback?.Invoke();
        }

        /// <summary>
        /// 入力が離れるときに実行
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text_Leave(object sender, System.EventArgs e)
        {
            Normalize(true);
        }

        /// <summary>
        /// キー入力制限イベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text_KeyPress(object sender, KeyPressEventArgs e)
        {
            //0～9と、バックスペース、aA~fF以外の時は、イベントをキャンセルする
            if ((e.KeyChar < '0' || '9' < e.KeyChar) && (e.KeyChar < 'A' || e.KeyChar > 'F') && (e.KeyChar < 'a' || e.KeyChar > 'f') && e.KeyChar != '\b')
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// 前回の入力をほかのテキストボックスに渡す
        /// 
        /// </summary>
        private void CurrentInputRepeat()
        {
            if (currentInput != null)
            {
                Text.Text = currentInput;
                Normalize();
            }
        }

        private static bool _isMouseLeftDowning;
        private static bool _isMouseRightDowning;
        private Point _befMousePos;
        private Point _nowMousePos;
        /// <summary>
        /// ポインターが乗ったら色を変更。
        /// ドラッグの検知でModeDecHex.Value値で上書きする。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClickTimer(object sender, EventArgs e)
        {
            if (!isDragAllow)
                return;

            var cursorPoint = PictureBox.PointToClient(Cursor.Position);
            _befMousePos = _nowMousePos;
            _nowMousePos = new Point(cursorPoint.X, cursorPoint.Y);

            _isMouseLeftDowning = CheckIsMouseLeft();
            _isMouseRightDowning = CheckIsMouseRight();

            if (_isMouseLeftDowning || _isMouseRightDowning)
                SetMassValueForInputText(cursorPoint);
        }

        /// <summary>
        /// 左クリックを監視
        /// </summary>
        /// <returns></returns>
        private bool CheckIsMouseLeft()
        {
            if ((Control.MouseButtons & MouseButtons.Left) == MouseButtons.Left)
                return true;
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 右クリックを監視
        /// </summary>
        /// <returns></returns>
        private bool CheckIsMouseRight()
        {
            if ((Control.MouseButtons & MouseButtons.Right) == MouseButtons.Right)
                return true;
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 入力値をドラッグしたマスにコピー。
        /// </summary>
        private void SetMassValueForInputText(System.Drawing.Point pnt)
        {
            // 単なるクリックのときは動かさない
            //if (MainForm._cursorPoint == _location)
            //    return;

            var isRide = PictureBox.ClientRectangle.Contains(pnt);
            if (isRide)
            {
                if (_isMouseLeftDowning)
                {
                    CurrentInputRepeat();
                }
                else
                {
                    Text.Text = "00";
                    Normalize(true);
                }
            }
        }

        /// <summary>
        /// ドラッグを許可する
        /// </summary>
        /// <param name="isAllow"></param>
        public static void AllowDrag(bool isAllow) 
        {
            isDragAllow = isAllow;
        }
    }
}
