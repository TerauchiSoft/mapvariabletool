﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public static class StringBuild
{
    private static StringBuilder _strBuilder;
    private static StringBuilder Instance { get { if (_strBuilder == null) { _strBuilder = new StringBuilder(); } return _strBuilder; } }
    public static StringBuilder GetStrBunler() { return _strBuilder; }
    public static string GetString() { return _strBuilder.ToString(); }
    public static string GetStringWithClear() {
        string str = _strBuilder.ToString();
        _strBuilder.Clear();
        return str;
    }

    public static StringBuilder ClearStr()
    {
        Instance.Clear();
        return Instance;
    }

    public static StringBuilder Append(string str)
    {
        Instance.Append(str);
        return Instance;
    }

    public static StringBuilder AppArgs(params string[] vs)
    {
        foreach (var s in vs)
            Instance.Append(s);
        return Instance;
    }

    public static string GetStringFromArgs(params string[] vs) {
        Instance.Clear();
        AppArgs(vs);
        return Instance.ToString();
    }

    public static StringBuilder Replace(string oldstr, string newstr)
    {
        Instance.Replace(oldstr, newstr);
        return Instance;
    }

    public static StringBuilder Replace(char oldchar, char newchar)
    {
        Instance.Replace(oldchar, newchar);
        return Instance;
    }

    public static StringBuilder Replace(string oldstr, string newstr, int startidx, int count)
    {
        Instance.Replace(oldstr, newstr, startidx, count);
        return Instance;
    }

    public static StringBuilder Replace(char oldchar, char newchar, int startidx, int count)
    {
        Instance.Replace(oldchar, newchar, startidx, count);
        return Instance;
    }

    public static StringBuilder Remove(int startidx, int count)
    {
        Instance.Remove(startidx, count);
        return Instance;
    }
}
